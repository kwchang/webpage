```
layout: publication
title: "Learning to Search for Dependencies"
key: "CHDL15"
authors: ["Kai-Wei Chang","He He", "Hal Daume; III", "John Lanford"]
year: 2015
booktitle: "Arxiv"
publish_type: arxiv
ispub: false
paper_url: "http://arxiv.org/abs/1503.05615"
software_url: "https://github.com/JohnLangford/vowpal_wabbit"
```
### Abstract
We demonstrate that a dependency parser can be built using a credit assignment compiler which removes the burden of worrying about low-level machine learning details from the parser implementation. The result is a simple parser which robustly applies to many languages that provides similar statistical and computational performance with best-to-date transition-based parsing approaches, while avoiding various downsides including randomization, extra feature requirements, and custom learning algorithms.

### Bib entry


> @inproceedings{CHDL15,<br>
>   author    = {Kai-Wei Chang and He He and Hal Daum\'e III and John Langford},<br>
>   title     = {Learning to Search for Dependencies}, <br>
>   booktitle = {Arxiv}, <br>
>   year      = {2015},<br>
>  }
