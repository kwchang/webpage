```
layout: publication
title: "The University of Illinois System in the CoNLL-2013 Shared Task"
key: "RCSR13"
authors: ["Alla Rozovskaya","Kai-Wei Chang","Mark Sammons","Dan Roth"]
year: 2013
booktitle: "CoNLL Shared Task"
publish_type: conference
ispub: true
keyword: ["Natural Language Processing","Shared Task"]
paper_url: "http://cogcomp.cs.illinois.edu/page/publication_view/731"
poster_url: "http://cogcomp.cs.illinois.edu/files/posters/ConllSharedTask2012.pptx"
```
### Abstract
The CoNLL-2013 shared task focuses on correcting grammatical errors in essays written by non-native learners of English. In this paper, we describe the University of Illinois system that participated in the shared task. The system consists of five components and targets five types of common grammatical mistakes made by English as Second Language writers. We describe our underlying approach, which relates to our previous work, and describe the novel aspects of the system in more detail. Out of 17 participating teams, our system is ranked first based on both the original annotation and on the revised annotation.

### Bib entry

> @inproceedings{RCSR13, <br>
> author = {Alla Rozovskaya and Kai-Wei Chang and Mark Sammons and Dan Roth}, <br>
> title= {{The University of Illinois System in the CoNLL-2013 Shared Task}}, <br>
> booktitle = {CoNLL Shared Task}, <br>
> year = {2013} <br>
> }
