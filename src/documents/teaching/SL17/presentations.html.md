```
title: "Presentations"
layout: doc_courseML
coursename: "CS6501-Advanced ML"
```


The purpose of this project presentation is to allow students to practice giving research talks in front of others. 
Imaging that you're presenting your work at a conference in front of people having basic understanding about NLP.

### Group
Each group has at most 3 students. Register your team at [spread sheet](https://docs.google.com/spreadsheets/d/1ZZRDd1-PRLhCPhkum9qysxI34SBLvUCRbgQY0vGtCyw/edit?usp=sharing) 

### Preparation 
Each presentation is 15-20 min (every group member has to present at least 3 min).
Students are required to prepare the slides by themselves (you cannot use slides from the original authors directly). However, feel free to use the motivative examples or
some parts of slides from the original authors (with a proper credit to the resources). Some papers are complicated, and you don't have to cover every part of the paper. 
Try to make your presentation simple and focus 
on the big idea of the paper.

### Grading

The presentation will be graded by the instructor, TA, and other students. The grading is based on the following factors:
- Slides content was clear
- Presenters were confident and understand the paper
- Presenters motivated the work well
- Presenters provided enough background to understand the work
- Presenters explained the key idea clearly
- Presenters provided insightful discussion about the work
- Presenters managed their time well
- Presenters answered questions well
- Overall, presenters did well


### Tips
[How to give good presentations](http://acmg.seas.harvard.edu/education/presentations/carlton_presentations.pdf)


### Schedule
Please see [Syllabus](http://www.cs.virginia.edu/~kc2wc/teaching/SL17/syllabus.html)
