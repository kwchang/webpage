```
layout: publication
title: "A Discriminative Latent Variable Model for Online Clustering"
key: "SamdaniChRo14"
authors: ["Rajhans Samdani","Kai-Wei Chang","Dan Roth"]
year: 2014
booktitle: "ICML"
publish_type: conference
ispub: true
keyword: ["Natural Language Processing","Structured Learning"]
paper_url: "http://cogcomp.cs.illinois.edu/page/publication_view/740"
demo_url: "http://bilbo.cs.uiuc.edu/~kchang10/"
```
### Abstract
This paper presents a latent variable structured prediction model for discriminative supervised clustering of items called the Latent Left-linking Model (L3M). We present an online clustering algorithm for L3M based on a feature-based item similarity function. We provide a learning framework for estimating the similarity function and present a fast stochastic gradient-based learning technique. In our experiments on coreference resolution and document clustering, L3M outperforms several existing online as well as batch supervised clustering techniques.

### Bib entry

> @inproceedings{SamdaniChRo14, <br>
> author = {Rajhans Samdani and Kai-Wei Chang and Dan Roth}, <br>
> title= {{A Discriminative Latent Variable Model for Online Clustering}}, <br>
> booktitle = {ICML}, <br>
> year = {2014} <br>
> }
