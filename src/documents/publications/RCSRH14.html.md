

```
layout: publication
title: "The Illinois-Columbia System in the CoNLL-2014 Shared Task"
key: "RCSRH14"
authors: ["Alla Rozovskaya","Kai-Wei Chang","Mark Sammons","Dan Roth","Nizar Habash"]
year: 2014
booktitle: "CoNLL Shared Task"
publish_type: conference
ispub: true
paper_url: "http://cogcomp.cs.illinois.edu/page/publication_view/746"
```
### Abstract


### Bib entry

> @inproceedings{RCSRH14, <br>
> author = {Alla Rozovskaya and Kai-Wei Chang and Mark Sammons and Dan Roth and Nizar Habash}, <br>
> title= {{The Illinois-Columbia System in the CoNLL-2014 Shared Task}}, <br>
> booktitle = {CoNLL Shared Task}, <br>
> year = {2014} <br>
> }
