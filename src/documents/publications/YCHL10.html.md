```
layout: publication
title: "A Comparison of Optimization Methods and software for Large-scale L1-regularized Linear Classification"
key: "YCHL10"
authors: ["Guo-Xun Yuan","Kai-Wei Chang","Cho-Jui Hsieh","Chih-Jen Lin"]
year: 2010
booktitle: "JMLR"
publish_type: journal
ispub: true
keyword: ["Large-Scale Learning","Machine Learning"]
paper_url: "../papers/YCHL10.pdf"
exp_url: "http://www.csie.ntu.edu.tw/~cjlin/liblinear/exp.html#lowpoly_exp"
software_url: "http://www.csie.ntu.edu.tw/~cjlin/libsvmtools/#fast_training_testing_for_degree_2_polynomial_mappings_of_data"
```
### Abstract
Large-scale linear classification is widely used in many areas. The L1-regularized form can
be applied for feature selection; however, its non-differentiability causes more difficulties in
training. Although various optimization methods have been proposed in recent years, these
have not yet been compared suitably. In this paper, we first broadly review existing methods.
Then, we discuss state-of-the-art software packages in detail and propose two efficient
implementations. Extensive comparisons indicate that carefully implemented coordinate
descent methods are very suitable for training large document data.

### Bib entry

> @article{YCHL10, <br>
> author = {Guo-Xun Yuan and Kai-Wei Chang and Cho-Jui Hsieh and Chih-Jen Lin}, <br>
> title= {{A Comparison of Optimization Methods and software for Large-scale L1-regularized Linear Classification}}, <br>
> journal = {JMLR}, <br>
> year = {2010} <br>
> }
