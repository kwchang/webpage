```
layout: publication
title: "Selective Algorithms for Large-Scale Classification and Structured Learning"
key: "Chang15"
authors: ["Kai-Wei Chang"]
year: 2015
booktitle: "UIUC Phd Thesis"
publish_type: "thesis"
ispub: true
paper_url: "http://cogcomp.cs.illinois.edu/page/publication_view/764"
```
### Abstract
The desired output in many machine learning tasks is a structured object, such as tree, clustering, or sequence. Learning accurate prediction models for such problems requires training on large amounts of data, making use of expressive features and performing global inference that simultaneously assigns values to all interrelated nodes in the structure. All these contribute to significant scalability problems. In this thesis, we describe a collection of results that address several aspects of these problems - by carefully selecting and caching samples, structures, or latent items.

Our results lead to entryfficient learning algorithms for large-scale binary classification models, structured prediction models and for online clustering models which, in turn, support reduction in problem size, improvements in training and evaluation speed and improved performance. We have used our algorithms to learn expressive models from large amounts of annotated data and achieve state-of-the art performance on several natural language processing tasks.

### Bib entry

> @techreport{Chang15, <br>
> author = {K.-W. Chang}, <br>
> title = {Selective Algorithms for Large-Scale Classification and Structured Learning}, <br>
> booktitle = {UIUC PhD Thesis}, <br>
>  year = {2015}  <br>
> }

