```
title: "Structured Predictions: Practical Advancements and Applications"
layout: doc
url: '/talk/sp.html'
urls: 
 - '/SP'
```

### Speaker: 
[Kai-Wei Chang](http://kwchang.net), Department of Computer Science @ University of Virgina


### Overview
 Many machine learning problems involve making joint predictions over a set of mutually dependent output variables. The dependencies between output variables can be represented by a structure, such as a sequence, a tree, a clustering of nodes, or a graph. Structured prediction models have been proposed for problems of this type, and they have been shown to be successful in many application areas, such as natural language processing, computer vision, and bioinformatics.
 There are two families of algorithms for these problems: graphical model approaches and learning to search approaches. In this talk, I will describe a collection of results that improve several aspects of these approaches. Our results lead to efficient learning algorithms for structured prediction models and for online clustering models, which, in turn, support reduction in problem size, improvements in training and evaluation speed, and improved performance. We have used our algorithms to learn expressive models from large amounts of annotated data and achieve state-of-the-art performance on several natural language processing tasks.

### Slides
[.pdf](sp17.pdf)

### Related Tutorials and Workshops
 - [AAAI16: Learning and Inference in Structured Prediction Models](http://cogcomp.cs.illinois.edu/page/tutorial.201602/)
 - [Tutorial: Hands-on Learning to Search for Structured Prediction Time](http://naacl.org/naacl-hlt-2015/tutorial-hands-on-learning.html), [slides, videos](http://hunch.net/~l2s/)
 - [EMNLP 16 Workshop on Structured Prediction for NLP](http://structuredprediction.github.io/EMNLP16)
 - [EMNLP 17 2nd Workshop on Structured Prediction for NLP](http://structuredprediction.github.io/EMNLP17)

### Software & Demos
 - [Illinois-SL](https://github.com/IllinoisCogComp/illinois-sl): Package for learning structured prediction models
 - [Vowpal Wabbit](https://github.com/JohnLangford/vowpal_wabbit): An online learning system (I contribute to the learning to search sub-system)
 - [Coreference Resolution Demo](http://cogcomp.cs.illinois.edu/page/demo_view/Coref)


### References

#### Coreference Resolution
 - [CoNLL15: A Joint Framework for Coreference Resolution and Mention Head Detection](http://www.cs.virginia.edu/~kc2wc/publications/PengChRo15.html)
 - [ICML14: A Discriminative Latent Variable Model for Online Clustering](http://www.cs.virginia.edu/~kc2wc/publications/SamdaniChRo14.html)
 - [EMNLP13: A Constrained Latent Variable Model for Coreference Resolution](http://www.cs.virginia.edu/~kc2wc/publications/ChangSaRo13.html)
 - [CoNLL12: Illinois-Coref: The UI System in the CoNLL-2012 Shared Task](http://www.cs.virginia.edu/~kc2wc/publications/CSRSR12.html)
 - [CoNLL11: Inference Protocols for Coreference Resolution](http://www.cs.virginia.edu/~kc2wc/publications/CSRRSR11.html)

#### Other NLP Applications
 - [CoNLL14: The Illinois-Columbia System in the CoNLL-2014 Shared Task](http://www.cs.virginia.edu/~kc2wc/publications/RCSRH14.html)
 - [CoNLL13: The University of Illinois System in the CoNLL-2013 Shared Task](http://www.cs.virginia.edu/~kc2wc/publications/RCSR13.html)
 - [EMNLP16: Learning from Explicit and Implicit Supervision Jointly For Algebra Word Problems](http://www.cs.virginia.edu/~kc2wc/publications/UCCY16.html)

#### Learning and Inference for Structured Prediction
 - [AAAI 17: Structured Prediction with Test-time Budget Constraints](http://www.cs.virginia.edu/~kc2wc/publications/BCWS16.html)
 - [NIP 16: A Credit Assignment Compiler for Joint Prediction](http://www.cs.virginia.edu/~kc2wc/publications/CHDLR16.html)
 - [AAAI 15: Structural Learning with Amortized Inference](http://www.cs.virginia.edu/~kc2wc/publications/CUKR15.html)
 - [ICML 15: Learning to Search Better Than Your Teacher](ICML15: http://www.cs.virginia.edu/~kc2wc/publications/CKADL15.html)
 - [NIPS OPT workshop 15: Distributed Training of Structured SVM](http://www.cs.virginia.edu/~kc2wc/publications/LCUR15.html)
 - [ECML 13: Tractable Semi-Supervised Learning of Complex Structured Prediction Models](http://www.cs.virginia.edu/~kc2wc/publications/ChangSuKe13.html)
 - [ECML 13: Multi-core Structural SVM Training](http://www.cs.virginia.edu/~kc2wc/publications/ChangSrRo13.html)

#### Implicit Supervision
 - [EMNLP16: Learning from Explicit and Implicit Supervision Jointly For Algebra Word Problems](http://www.cs.virginia.edu/~kc2wc/publications/UCCY16.html)
 - [ICML 15: Learning to Search Better Than Your Teacher](ICML15: http://www.cs.virginia.edu/~kc2wc/publications/CKADL15.html)

#### Social Biases in Learning Models
 - [NIP16: Man is to Computer Programmer as Woman is to Homemaker? Debiasing Word Embeddings](Man is to Computer Programmer as Woman is to Homemaker? Debiasing Word Embeddings)


