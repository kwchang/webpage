```
layout: publication
title: "Illinois-Coref: The UI System in the CoNLL-2012 Shared Task"
key: "CSRSR12"
authors: ["Kai-Wei Chang","Rajhans Samdani","Alla Rozovskaya","Mark Sammons","Dan Roth"]
year: 2012
booktitle: "CoNLL Shared Task"
publish_type: conference
ispub: true
keyword: ["Natural Language Processing","Shared Task"]
poster_url: "http://cogcomp.cs.illinois.edu/files/posters/ConllSharedTask2012.pptx"
paper_url: "http://cogcomp.cs.illinois.edu/papers/CSRSR12.pdf"
```
### Abstract
The CoNLL-2012 shared task is an extension of the last year��s coreference task. We participated in the closed track of the shared tasks in both years. In this paper, we present the improvements of Illinois-Coref system from last year. We focus on improving mention detection and pronoun coreference resolution, and present a new learning protocol. These new strategies boost the performance of the system by 5% MUC F1, 0.8% BCUB F1, and 1.7% CEAF F1 on the OntoNotes-5.0 development set.

### Bib entry

> @inproceedings{CSRSR12, <br>
> author = {Kai-Wei Chang and Rajhans Samdani and Alla Rozovskaya and Mark Sammons and Dan Roth}, <br>
> title= {{Illinois-Coref: The UI System in the CoNLL-2012 Shared Task}}, <br>
> booktitle = {CoNLL Shared Task}, <br>
> year = {2012} <br>
> }
