```
title: "Resources"
layout: doc_courseML
coursename: "CS6501-Advanced ML"
```
# Textbook/references
The following textbook is recommended but not required. 

- Yoav Goldberg (2015), [A Primer on Neural Network Models for Natural Language Processing](http://u.cs.biu.ac.il/~yogo/nnlp.pdf)
- Noah A. Smith (2011), [Linguistic Structure Prediction](http://www.cs.cmu.edu/~nasmith/LSP/)
- Sebastian Nowozin and Christoph H. Lampert (2011), [Structured Learning and Prediction in Computer Vision](http://www.nowozin.net/sebastian/papers/nowozin2011structured-tutorial.pdf)

### Required readings
- Ch2: Chih-Wei Hsu and Chih-Jen Lin, A Comparison of Methods for Multi-class Support Vector Machines
- Ch3: Lawrence Rabiner, A tutorial on hidden Markov models and selected applications in speech recognition, Proceedings of IEEE, 1989. 
- Ch3: Michael Collins, notes on [Log-Linear Models, MEMMs, and CRFs](http://www.cs.columbia.edu/~mcollins/crf.pdf)
- Ch4: Daphne Koller, Nir Friedman, Lise Getoor and Ben Taskar, [Graphical Models in a Nutshell](http://ai.stanford.edu/~koller/Papers/Koller%2Bal:SRL07.pdf)
- Ch4: Hal Daume III, John Lanford, Kai-Wei Chang, He He, and Sudha Rao, [Tutorail on Hands-on Learning to Search for Structured Prediction](http://techtalks.tv/talks/hands-on-learning-to-search-for-structured-prediction/61566/)
- Ch5: Dan Roth and Wen-tau Yih, Integer Linear Programming Inference for Conditional Random Fields, ICML 2005.
- Ch5: David Sontag, Approximate Inference in Graphical Models using LP Relaxations, [talk](Approximate Inference in Graphical Models using LP Relaxations), [thesis](http://www.cs.nyu.edu/~dsontag/papers/sontag_phd_thesis.pdf)
- Ch5: Alexander Rush and Michael Collins, A Tutorial on Dual Decomposition and Lagrangian Relaxation for Inference in Natural Language Processing, JAIR 2012.
- Ch6: Nati Srebro and Ambuj Tewari, Stochastic Optimization for Machine Learning [talk slide](http://ttic.uchicago.edu/~nati/Publications/ICML10tut.pdf)
- Ch7: Yoav Goldberg, [A Primer on Neural Network Models for Natural Language Processing](http://u.cs.biu.ac.il/~yogo/nnlp.pdf)


### Recommended readings
- Multiclass classification
  - Andre Elisseeff and Jason Weston, A kernel method for multi-labelled classification, NIPS 2001
  - Koby Crammer and Yoram Singer, On the Algorithmic Implementation of Multiclass Kernel-based Vector Machines, JMLR 2001
  - Sariel Har-Peled, Dan Roth, Dav Zimak, Constraint classification: A new approach to multiclass classification, ALT 2002
  - Thomas G. Dietterich and Ghulum Bakiri, Solving Multiclass Learning Problems via  Error-Correcting Output Codes, JAIR 1995
  - Alina Beygelzimer, 	John Langford, and Pradeep Ravikumar, Error-correcting tournaments, ALT 2009
  - Erin L. Allwein, Robert E. Schapire, Yoram Singer, Reducing Multiclass to Binary: A Unifying Approach for Margin Classifiers, ICML 2000. 
- Sequential Model
  - Andrew McCallum, Dayne Freitag and Fernando Pereira, Maximum Entropy Markov Models for Information Extraction and Segmentation, ICML 2000.
  - John Lafferty, Andrew McCallum and Fernando Pereira, Conditional Random Fields: Probabilistic Models for Segmenting and Labeling Sequence Data, ICML 2001.
  - Michael Collins, Discriminative Training for Hidden Markov Models: Theory and Experiments with Perceptron Algorithms, EMNLP 2002
  - Michael Collins and Brain Roark, Incremental Parsing with the Perceptron Algorithm, ACL 2004
																					  
- Structrued Inference
  - Vasin Punyakanok, Dan Roth, Wen-tau Yih and Dav Zimak, Learning and Inference over Constrained Output, IJCAI 2005.
  - Alexander Rush and Michael Collins, [A Tutorial on Dual Decomposition and Lagrangian Relaxation for Inference in Natural Language Processing](https://www.jair.org/media/3680/live-3680-6584-jair.pdf), JAIR 2012
  - Sameer Singh, Michael Wick and Andrew McCallum, Monte Carlo MCMC: Efficient Inference by Approximate Sampling, EMNLP 2012
  - Vivek Srikumar, Gourab Kundu and Dan Roth, On Amortizing Inference Cost for Structured Prediction, EMNLP 2012.

- Structured Learning
  - Liang Huang, Suphan Fayong, Yang Guo, Structured Perceptron with Inexact Search, NAACL 2012
  - Ben Taskar, Carlos Guestrin, and Daphne Koller. Max-margin Markov networks, NIPS 2004
  - Ioannis Tsochantaridis, Thomas Hofmann, Thorsten Joachims, and Yasemin Altun. Support vector machine learning for interdependent and structured output spaces, ICML 2004
  - Simon Lacoste-Julien, Martin Jaggi, Mark Schmidt and Patrick Pletscher, Block-Coordinate Frank-Wolfe Optimization for Structural SVMs, ICML 2013
  - Kai-Wei Chang, Akshay Krishnamurthy, Alekh Agarwal, Hal Daume III, John Langford, Learning to Search Better Than Your Teacher, ICML 2015
- Deep Learning 
  - Ronan Collobert, Jason Weston, Leon Bottou, Michael Karlen, Koray Kavukcuoglu and Pavel Kuksa. Natural Language Processing (Almost) from Scratch. JMLR 2011.
  - Richard Socher, John Bauer, Christopher D. Manning, and Andrew Y. Ng, Parsing With Compositional Vector Grammars. ACL 2013.
  - Yoshua Bengio, Deep Learning of Representations: Looking Forward. 2013.
  - Ilya Sutskever, Oriol Vinyals, Quoc V. Le, Sequence to Sequence Learning with Neural Networks, NIPS 2014.
- Others
  -  Ming-Wei Chang, Lev Ratinov, and Dan Roth, Guiding Semi-Supervision with Constraint-Driven Learning ACL 2007.
  - Kuzman Ganchev, Joao Graca, Jennifer Gillenwater and Ben Taskar, Posterior Regularization for Structured Latent Variable Models, JMLR, 2010.
  - Percy Liang, Hal Daume, and Dan Klein. Structure Compilation: Trading Structure for Features. ICML 2008. ([PDF](https://cs.stanford.edu/~pliang/papers/structure-icml2008.pdf))

### Software

Off-the-shelf deep learning libraries (note that they are in different abstraction levels)
- [Tensorflow](https://www.tensorflow.org/)
- [Theano](http://deeplearning.net/software/theano/)
- [Keras](https://keras.io/)
- [Lasagne](https://lasagne.readthedocs.io/en/latest/)
- [CMU Dynamic NN library](https://github.com/clab/cnn) and its [NLP applications](https://github.com/clab)

Structured learning libraries:
- [Wolfe](http://www.wolfe.ml/)
- [Saul](https://github.com/IllinoisCogComp/saul)
- [Dyna](https://github.com/nwf/dyna)
- [PyStruct](https://pystruct.github.io/)
- [Learning to search in VW](https://github.com/JohnLangford/vowpal_wabbit)


