```
layout: publication
title: "Inference Protocols for Coreference Resolution"
key: "CSRRSR11"
authors: ["Kai-Wei Chang","Rajhans Samdani","Alla Rozovskaya","Nick Rizzolo","Mark Sammons","Dan Roth"]
year: 2011
booktitle: "CoNLL Shared Task"
publish_type: conference
ispub: true
keyword: ["Natural Language Processing","Shared Task"]
paper_url: "http://cogcomp.cs.illinois.edu/page/publication_view/669"
poster_url: "http://cogcomp.cs.illinois.edu/files/posters/ConllSharedTask2011_final.pptx"
slides_url: "http://cogcomp.cs.illinois.edu/files/presentations/Inference%20Protocols%20for%20Coreference%20Resolution.pptx"
```
### Abstract
This paper presents Illinois-Coref, a system for coreference resolution that participated in the CoNLL-2011 shared task. We investigate two inference methods, Best-Link and All-Link, along with their corresponding, pairwise and structured, learning protocols. Within these, we provide a flexible architecture for incorporating linguistically-motivated constraints, several of which we developed and integrated. We compare and evaluate the inference approaches and the contribution of constraints, analyze the mistakes of the system, and discuss the challenges of resolving coreference for the OntoNotes-4.0 data set.

### Bib entry

> @inproceedings{CSRRSR11, <br>
> author = {Kai-Wei Chang and Rajhans Samdani and Alla Rozovskaya and Nick Rizzolo and Mark Sammons and Dan Roth}, <br>
> title= {{Inference Protocols for Coreference Resolution}}, <br>
> booktitle = {CoNLL Shared Task}, <br>
> year = {2011} <br>
> }
