```
title: "Schedule"
layout: doc_courseML
coursename: "CS6501-Advanced ML"
```
The following schedule is subjected to change. 


 Lecture | Date | topic | Slides | Due | Readings
:------- | :---- | :----- | :----- | :-----: | :----:
Ch1. Overview | Wed 1/18 | Introduction | [.pptx](01-intro.pptx),[.pdf](01-intro.pdf) | [Hw0](hw0.pdf) (not graded)   |  
Ch2. Review | Mon 1/23 | Binary classification | [.pptx](02-binary.pptx), [.pdf](02-binary.pdf) | |
 | Wed 1/25 |Loss minimization |  continue |  | 
 | Mon 1/30 | Multiclass classification | [.pptx](03-multiclass.pptx), [.pdf](03-multiclass.pdf) | | [Multiclass SVM](https://www.csie.ntu.edu.tw/~cjlin/papers/multisvm.pdf)
 | Wed 2/01 | Multiclass classification | continue | | |
 | Mon 2/06 | Neural network review | [.pptx](04-nn.pptx), [.pdf](04-nn.pdf) | [Hw1](cs6501_ML_hw1.pdf), [source](cs6501_hw1.zip) [solution](hw1_solution.pdf) | [Neural Networks](http://u.cs.biu.ac.il/~yogo/nnlp.pdf), Sec 1-4
Ch3. Sequential models | Mon 2/13 | First Look at Structures | [.pptx](05-structure.pptx), [.pdf](05-structure.pdf) | | 
 | Wed 2/15 | HMM |[.pptx](06-sequence.pptx),[.pdf](06-sequence.pdf) | | [HMM](http://www.ece.ucsb.edu/Faculty/Rabiner/ece259/Reprints/tutorial%20on%20hmm%20and%20applications.pdf)
 | Fri 2/17 | MEMM  | | Hw1 due (Sat) | [Log-linear model](http://www.cs.columbia.edu/~mcollins/crf.pdf)
Ch4. General formulations | Mon 2/20 | CRF |  | | [graphical models](http://ai.stanford.edu/~koller/Papers/Koller%2Bal:SRL07.pdf)
 | Wed 2/22 |  More on CRF| | [Quiz1 Solution](quiz1.pdf)
 | Mon 2/27 |  Graphical models |[.pptx](06-graphical.pptx), [.pdf](06-graphical.pdf) | | [Hal's talk](http://techtalks.tv/talks/hands-on-learning-to-search-for-structured-prediction/61566/)
 | Wed 3/01 | Constrainted Conditional Model | [.pptx](07-ccm.pptx), [.pdf](07-ccm.pdf)| | 
 | Mon 3/06 | Spring break | Spring break  | [HW2](cs6501_hw2.pdf) [source](hw2_source.zip) | 
 | Wed 3/08 | Spring break | Spring break |  | 
  Ch5. Inference | Mon 3/13 | Inference and ILP  |[.pptx](08-inference.pptx), [.pdf](08-inference.pdf) |  | [ILP inference](http://l2r.cs.uiuc.edu/~danr/Papers/RothYi05.pdf)
 | Wed 3/15 |  Integer linear programming   |  | Hw2 Due, [solution](hw2_solution.pdf) | [Sontag's talk](https://www.youtube.com/watch?v=JnkgDUS2VrQ)
 | Mon 3/20 | Learning to search | | [Quiz2 Solution](quiz2.pdf) | 
 | Wed 3/22 | Belief propagation | | | [Dual Decomposition](https://www.jair.org/media/3680/live-3680-6584-jair.pdf) 
 | Mon 3/27 | Approximate inference | | Proposal due| 
  Ch6. Training| Wed 3/29 | Training strategies | [.pptx](09-learning.pptx), [.pdf](09-learning.pdf) | |  [Stochastic optimization](http://ttic.uchicago.edu/~nati/Publications/ICML10tut.pdf)
 | Mon 4/03 | Other learning protocols | [.pptx](10-Other.pptx), [.pdf](10-Other.pdf) | | 
 Ch7. Deep learning| Wed 4/05 | CNN | |  | [Neural Networks](http://u.cs.biu.ac.il/~yogo/nnlp.pdf), Sec 5-12. 
 | Mon 4/10 | Recurrent NN, LSTM | | | 
 | Wed 4/12 | Seq-to-seq learning | | | Documentations of one of DL packages
  Ch7. Additional topics| Mon 4/17 | Application in NLP | | | 
| Wed 4/19 | Summary |[.pptx](11-Summary.pptx), [.pdf](11-Summary.pdf) | [Hw3](ML-hw3.pdf)    | 
 | Mon 4/24 | Final Presentation | |  | 
 | Wed 4/26 | Final Presentation | | Hw3 due [solution](hw3_sol.pdf)| 
 | Mon 5/01 | Learning with Latent Variable |  | Quiz3 [solution](quiz3-solution.pdf) | 



 Presentations:

 Date | Paper
:------- | :---- 
2/1 | Thomas G. Dietterich and Ghulum Bakiri, Solving Multiclass Learning Problems via  Error-Correcting Output Codes, JAIR 1995
2/8 | Andre Elisseeff and Jason Weston, A kernel method for multi-labelled classification, NIPS 2001
2/13 | Michael Collins, Discriminative Training for Hidden Markov Models: Theory and Experiments with Perceptron Algorithms, EMNLP 2002
2/15 | Kevin Gimpel Noah A. Smith, Softmax-Margin CRFs: Training Log-Linear Models with Cost Functions 
2/22 | Sariel Har-Peled, Dan Roth, Dav Zimak, Constraint classification: A new approach to multiclass classification, ALT 2002
2/27 | Hal Daume and Daniel Marcu, Learning as Search Optimization: Approximate Large Margin Methods for Structured Prediction
3/01 | Vivek Srikumar, Gourab Kundu and Dan Roth, On Amortizing Inference Cost for Structured Prediction, EMNLP 2012.
3/13 | Vasin Punyakanok, Dan Roth, Wen-tau Yih and Dav Zimak, Learning and Inference over Constrained Output, IJCAI 2005.
3/15 | Sameer Singh, Michael Wick and Andrew McCallum, Monte Carlo MCMC: Efficient Inference by Approximate Sampling, EMNLP 2012.
3/20 | Sontag, D., T. Meltzer, A. Globerson, T. Jaakkola, and Y. Weiss. Tightening LP Relaxations for MAP using Message Passing. UAI
3/22 | Alexander M. Rush, David Sontag, Michael Collins, and Tommi Jaakkola, On Dual Decomposition and Linear Programming Relaxations for Natural Language Processing, EMNLP 2010
3/29 | Amir Globerson, Terry Koo, Xavier Carreras, and Michael Collins. Exponentiated Gradient Algorithms for Log-Linear Structured Prediction. ICML 2007
4/03 | Ming-Wei Chang Lev Ratinov Dan Roth, Guiding Semi-Supervision with Constraint-Driven Learning, ACL 2007
4/05 | Richard Socher, John Bauer, Christopher D. Manning, and Andrew Y. Ng, Parsing With Compositional Vector Grammars. ACL 2013.
4/05 | Yoshua Bengio, Rejean Ducharme, Pascal Vincent, and Christian Jauvin. A Neural Probabilistic Language Model. ICML 2003.
4/10 | Sam Wiseman, Alexander M. Rush, Sequence-to-Sequence Learning as Beam-Search Optimization, EMNLP 2016.
4/12 | Sam Wiseman, Alexander M. Rush, and Stuart M. Shieber. Learning Global Features for Coreference Resolution, NAACL 2016 
4/17 | Dzmitry Bahdanau, KyungHyun Cho, Yoshua Bengio Neural Machine Translation by Jointly Learning to Align and Translate. ICLR 2015.
4/19 | Andrej Karpathy and  Li Fei-Fei, Deep visual-semantic alignments for generating image descriptions, CVPR 2015
4/19 | Ian J. Goodfellow, Jean Pouget-Abadie, Mehdi Mirza, Bing Xu, David Warde-Farley, Sherjil Ozair, Aaron Courville, Yoshua Bengio, Generative Adversarial Networks, NIPS 2014.
5/1 | Chun-Nam John Yu  and Thorsten Joachims, Learning Structural SVMs with Latent Variables, ICML 2009.

