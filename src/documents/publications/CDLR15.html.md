```
layout: publication
title: "Efficient programmable learning to search"
key: "CDLR15"
authors: ["Kai-Wei Chang","Hal Daume; III", "John Lanford", "Stephane Ross"]
year: 2015
booktitle: "Arxiv"
publish_type: "arxiv"
ispub: false
paper_url: "http://arxiv.org/abs/1406.1837"
software_url: "https://github.com/JohnLangford/vowpal_wabbit"
```
### Abstract
We improve "learning to search" approaches to structured prediction in two ways. First, we show that the search space can be defined by an arbitrary imperative program, reducing the number of lines of code required to develop new structured prediction tasks by orders of magnitude. Second, we make structured prediction orders of magnitude faster through various algorithmic improvements. We demonstrate the feasibility of our approach on three structured prediction tasks: two variants of sequence labeling and entity-relation resolution. In all cases we obtain accuracies at least as high as alternative approaches, at drastically reduced execution and programming time.

### Bib entry


> @inproceedings{CHLR15,<br>
>   author    = {Kai-Wei Chang and Hal Daum\'e III and John Langford and Stephane Ross},<br>
>   title     = {Efficient programmable learning to search}, <br>
>   booktitle = {Arxiv}, <br>
>   year      = {2015},<br>
>  }
