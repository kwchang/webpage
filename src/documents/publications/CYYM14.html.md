```
layout: publication
title: "Typed Tensor Decomposition of Knowledge Bases for Relation Extraction"
key: "CYYM14"
authors: ["Kai-Wei Chang","Wen-tau Yih","Bishan Yang","Chris Meek"]
year: 2014
booktitle: "EMNLP"
publish_type: conference
ispub: true
keyword: ["Natural Language Processing"]
paper_url: "http://research.microsoft.com/pubs/226677/main-trescal.pdf"
```
### Abstract
While relation extraction has traditionally been viewed as a task relying solely on textual data, recent work has shown that by taking as input existing facts in the form of entity-relation triples from both knowledge bases and textual data, the performance of relation extraction can be improved significantly. Following this new paradigm, we propose a tensor decomposition approach for knowledge base embedding that is highly scalable, and is especially suitable for relation extraction. By leveraging relational domain knowledge about entity type information, our learning algorithm is significantly faster than previous approaches and is better able to discover new relations missing from the database. In addition, when applied to a relation extraction task, our approach alone is comparable to several existing systems, and improves the weighted mean average precision of a state-of-the-art method by 10 points when used as a subcomponent.

### Bib entry

> @inproceedings{CYYM14, <br>
> author = {Kai-Wei Chang and Wen-tau Yih and Bishan Yang and Chris Meek}, <br>
> title= {{Typed Tensor Decomposition of Knowledge Bases for Relation Extraction}}, <br>
> booktitle = {EMNLP}, <br>
> year = {2014} <br>
> }
