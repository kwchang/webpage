```
layout: publication
title: "Large Linear Classification When Data Cannot Fit In Memory"
key: "YHCL10"
authors: ["Hsiang-Fu Yu","Cho-Jui Hsieh","Kai-Wei Chang","Chih-Jen Lin"]
year: 2012
booktitle: "TKDD"
publish_type: journal
ispub: true
iskeypub: true
keyword: ["Large-Scale Learning","Machine Learning"]
paper_url: "http://www.csie.ntu.edu.tw/~cjlin/papers/disk_decomposition/tkdd_disk_decomposition.pdf"
exp_url: "http://www.csie.ntu.edu.tw/~cjlin/liblinear/exp.html#lowpoly_disk_decomposition"
keynote: "Best Paper Award, KDD 10"
note: "(An earlier version appears in KDD 2010, an short version is published in IJCAI 2011 best paper track)"
```
### Abstract
Recent advances in linear classification have shown that for
applications such as document classification, the training
can be extremely efficient. However, most of the existing
training methods are designed by assuming that data can
be stored in the computer memory. These methods cannot
be easily applied to data larger than the memory capacity
due to the random access to the disk. We propose and analyze
a block minimization framework for data larger than
the memory size. At each step a block of data is loaded
from the disk and handled by certain learning methods. We
investigate two implementations of the proposed framework
for primal and dual SVMs, respectively. As data cannot fit
in memory, many design considerations are very different
from those for traditional algorithms. Experiments using
data sets 20 times larger than the memory demonstrate the
effectiveness of the proposed method.

### Bib entry

> @inproceedings{YHCL10, <br>
> author = {Hsiang-Fu Yu and Cho-Jui Hsieh and Kai-Wei Chang and Chih-Jen Lin}, <br>
> title= {{Large Linear Classification When Data Cannot Fit In Memory}}, <br>
> booktitle = {KDD}, <br>
> year = {2010} <br>
> }

### Notes
- A earlier version appears in KDD 2010 and receives *Best Research Paper Award*. [KDD paper](http://www.csie.ntu.edu.tw/~cjlin/papers/kdd_disk_decomposition.pdf). 
- Another simplified version is published in IJCAI 2011 (Best paper track). [IJCAI paper](http://www.cs.illinois.edu/~kchang10/ijcai_disk_decomposition.pdf) [Video](http://ijcai-11.iiia.csic.es/video/112) 
- Here is a 5-min introduction of this work. [Video](http://www.youtube.com/watch?v=hNCmIh_nOC0)
