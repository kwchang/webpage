###
layout: default
###

docsCollection = @getCollection('docs')
for item,index in docsCollection.models
	if item.id is @document.id
		break

div '.topbar', ->
	div '.topnav', ->
		div '.links', ->
			a '.logo.secondary', href: 'http://kwchang.net/teaching/SL17', title: @document.coursename, ->
				@document.coursename

			for own page, url of @navigation.courseSL
				a '.primary', href: url, -> text page

#		text @partial 'content/search'

#	nav '.sidebar', ->
#		text @partial('menu/menu.html.coffee',{
#			collection: docsCollection
#			activeItem: @document
#			partial: @partial
#			moment: @moment
#			underscore: @underscore
#			getCategoryName: @getCategoryName
#		})

div '.mainbar', ->
	text @content

footer '.bottombar', ->
	div '.about', -> @text['copyright']
	div '.links', ->
		for own page, url of @navigation.bottom
			a href: url, -> page
