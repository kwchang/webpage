```
title: "Old News"
layout: doc
```
### Not-so-recent News

#### 2018
- 04.2018, Congrats Wasi Ahmad on his [SIGIR paper](publications/ACW18a.html) and Muhao Chen on his [IJCAI paper](publications/CTCSZ.html) 
- 04.2018, Congrats Jieyu Zhao on winning the best poster award at SoCal NLP Symposium 2018.
- 04.2018, Check out our new NAACL paper on [Gender Bias in Coreference Resolution](publications/ZWYOC18.html).
- 04.2018, Check out our new NAACL paper on [LearningWord Embeddings for Low-resource Languages by PU Learning](publications/JYHC18.html).
- 01.2018, We will give a tutorial on Quantifying and Reducing Gender Stereotypes in Word Embeddings at [FAT 18](http://fatconference.org). 
- 01.2018, Check out [our paper](publications/ACW18.html) accepted by ICLR 2018.
- 12.2017, The slides of my tutotiral at TAAI 17' can be found [here](TAAI17.pdf)
- 08.2017, Our paper on [reducing gender bias](publications/ZWYOC.html) won the **best long paper award** at EMNLP 17.
- 07.2017, Our paper on [multi-sense embedding](publications/UCTKZ17.html) won the **best paper award** at [Rep4NLP](https://sites.google.com/site/repl4nlp2017/accepted-papers) workshop at ACL 17.
- 05.2017, I will be joining the [UCLA-CS](www.cs.ucla.edu) this Fall. I've enjoyed every moment at UVa, where the department and my collaborators gave me enormous support to start my career. I'm excited about the next adventure and looking forward to collaborating with excellent folks in SoCal.

#### 2017

- 12.2017, The slides of my tutotiral at TAAI 17 can be found [here](TAAI17.pdf)
- 08.2017, Our paper on [reducing gender bias](publications/ZWYOC.html) won the **best long paper award** at EMNLP 17.
- 07.2017, Our paper on [multi-sense embedding](publications/UCTKZ17.html) won the **best paper award** at [Rep4NLP](https://sites.google.com/site/repl4nlp2017/accepted-papers) workshop at ACL 17.
- 05.2017, I will be joining the [UCLA-CS](www.cs.ucla.edu) this Fall. I've enjoyed every moment at UVa, where the department and my collaborators gave me enormous support to start my career. I'm excited about the next adventure and looking forward to collaborating with excellent folks in SoCal.
- 05.2017, Check out our workshop on [Structured Prediction for NLP](structuredprediction.github.io) at EMNLP 2017.
- 05.2017, Check out our workshop on [Deep Structured Prediction](deepstruct.github.io) at ICML 2017.
- 03.2017, My proposal, [CRII: Learning Structured Prediction Models with Auxiliary Supervision](https://www.nsf.gov/awardsearch/showAward?AWD_ID=1657193&HistoricalAwards=false) has been funded by NSF.
- 01.2017, Congratulate Wasi Ahmad on being awarded the William L Ballard Jr. Fellowship for Spring of 2017.


#### 2016
- 12.06.2016, I gave an invited talk at NIPS 16 Workshop on [Learning in High Dimensions with Structure](https://sites.google.com/site/structuredlearning16/schedule) and [VW tutorial](https://github.com/JohnLangford/vowpal_wabbit/wiki/Tutorial) at Machine Learning System Workshop
- 11.28.2016, Our paper is accepted by AAAI 17: [Structured Prediction with Test-time Budget Constraints](http://www.cs.virginia.edu/~kc2wc/publications/BCWS16.html)
- 11.08.2016, Thanks all participants for a successful workshop. See you in the Second Workshop on Structured Prediction for NLP at EMNLP 2017 in Copenhagen! 
- 08.12.2016, [Our paper](http://arxiv.org/abs/1607.06520) about debiasing wordembedings is reported by [NPR](http://goo.gl/vnTAjz) and [MIT Technology Review](https://goo.gl/6zJVrC).
- 08.12.2016, Two papers accepted by NIPS! [Credit assignment compiler for L2S](http://goo.gl/PLWQM0); [Debiasing wordembeddings](http://goo.gl/Sc120F).
- 08.10.2016, I join [the Computer Science Department at University of Virginia](http://www.cs.virginia.edu) as an Assistant Professor!
- 08.01.2016, One paper accepted by EMNLP: Learning from Explicit and Implicit Supervision Jointly For Algebra Word Problems 
- 02.13.2016, Checkout our tutorial on [Structured Prediction](http://cogcomp.cs.illinois.edu/page/tutorial.201602/) at AAAI16.

