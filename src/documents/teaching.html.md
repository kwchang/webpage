```
title: "Teaching"
layout: doc
```
### Winter 2018, [CS M146: Introduction to Machine Learning](https://uclanlp.github.io/CSM146-18W), UCLA
### Fall 2017, [CS269]: [Seminar: Current Topics in Artificial Intelligence: Machine Learning in Natural Language Processing](https://uclanlp.github.io/CS269-17/), UCLA
### Spring 2017, [CS6501: Advanced Machine Learning -- Structured Prediction and Deep Learning ](http://kwchang.net/teaching/SL17), University of Virginia
### Fall 2016, [CS6501: Natural Language Processing](http://kwchang.net/teaching/NLP16), University of Virginia

