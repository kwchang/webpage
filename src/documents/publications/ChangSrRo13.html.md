```
layout: publication
title: "Multi-core Structural SVM Training"
key: "ChangSrRo13"
authors: ["Kai-Wei Chang","Vivek Srikumar","Dan Roth"]
year: 2013
booktitle: "ECML"
publish_type: conference
ispub: true
keyword: ["Structured Learning","Machine Learning"]
```
### Abstract

Many problems in natural language processing and computer vision can be framed as structured prediction problems. Structural support vector machines (SVM) is a popular approach for training structured predictors, where learning is framed as an optimization problem. Most structural SVM solvers alternate between a model update phase and an inference phase (which predicts structures for all training examples). As structures become more complex, inference becomes a bottleneck and thus slows down learning considerably.  In this paper, we propose a new learning algorithm for structural SVMs called DEMI-DCD that extends the dual coordinate descent approach by decoupling the model update and inference phases into different threads. We take advantage of multi-core hardware to parallelize learning with minimal synchronization between the model update and the inference phases. We prove that our algorithm not only converges but also fully utilizes all available processors to speed up learning, and validate our approach on two real-world NLP problems: part-of-speech tagging and relation extraction.  In both cases, we show that our algorithm utilizes all available processors to speed up learning and achieves competitive performance. For example, it achieves a relative duality gap of 1% on a POS tagging problem in 192 seconds using 16 threads, while a standard implementation of a multi-threaded dual coordinate descent algorithm with the same number of threads requires more than 600 seconds to reach a solution of the same quality.

### Bib entry

> @inproceedings{ChangSrRo13, <br>
> author = {Kai-Wei Chang and Vivek Srikumar and Dan Roth}, <br>
> title= {{Multi-core Structural SVM Training}}, <br>
> booktitle = {ECML}, <br>
> year = {2013} <br>
> }:w

