```
layout: publication
title: "A Joint Framework for Coreference Resolution and Mention Head Detection"
key: "PengChRo15"
authors: ["Haoruo Peng", "Kai-Wei Chang","Dan Roth"]
year: 2015
booktitle: "CoNLL"
publish_type: conference
ispub: true
paper_url: "http://cogcomp.cs.illinois.edu/page/publication_view/771"
```
### Abstract
In coreference resolution, a fair amount of research treats mention detection as a preprocessed step and focuses on developing algorithms for clustering coreferred mentions. However, there are significant gaps between the performance on gold mentions and the performance on the real problem, when mentions are predicted from raw text via an imperfect Mention Detection (MD) module. Motivated by the goal of reducing such gaps, we develop an ILP-based joint coreference resolution and mention head formulation that is shown to yield significant improvements on coreference from raw text, outperforming existing state-of-art systems on both the ACE-2004 and the CoNLL-2012 datasets. At the same time, our joint approach is shown to improve mention detection by close to 15% F1. One key insight underlying our approach is that identifying and co-referring mention heads is not only sufficient but is more robust than working with complete mentions.

### Bib entry
> @inproceedings{PengChRo15, <br>
>     author = {Haoruo Peng and Kai-Wei Chang and Dan Roth},<br>
>     title = {A Joint Framework for Coreference Resolution and Mention Head Detection},<br>
>     booktitle = {CoNLL},<br>
>     year = {2015},<br>
> }
