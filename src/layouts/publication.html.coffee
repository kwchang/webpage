###
layout: structure
###

paper_url = ""
paper_url = @document.paper_url if @document.paper_url? 
demo_url = ""
demo_url = @document.demo_url if @document.demo_url? 
poster_url = ""
poster_url = @document.poster_url if @document.poster_url? 
slides_url = ""
slides_url = @document.slides_url if @document.slides_url? 
software_url = ""
software_url = @document.software_url if @document.software_url? 
exp_url = ""
exp_url = @document.exp_url if @document.exp_url? 

section '#content', ->
	div '.page', ->
		article ".block doc", ->
			header ".block-publication",->
				a '.permalink.hover-link', href:@getUrl(@document), ->
					h2 h(@document.title)
				h4 h(@document.authors.join(", "))								

	
				publisher = @document.booktitle
				publisher += " " + @document.year if (@document.publish_type == 'conference')	
				h4 h(publisher)				

			section ".block-publication-content", ->
				h3 h("Links")
				ul ->
					li "<a href='"+paper_url+"'> Full text </a>" if (paper_url != '')				
					li "<a href='"+slides_url+"'> Presentation (.pdf|.pttx)</a>" if (slides_url != '')
					li "<a href='"+poster_url+"'> Poster (.pdf|.pttx)</a>" if (poster_url != '')
					li "<a href='"+demo_url+"'> Demo </a>" if (demo_url != '')
					li "<a href='"+software_url+"'> Software </a>" if (software_url != '')
					li "<a href='"+exp_url+"'> Experiment Code </a>" if (exp_url != '')


				@content
