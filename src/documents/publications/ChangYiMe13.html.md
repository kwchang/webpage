```
layout: publication
title: "Multi-Relational Latent Semantic Analysis"
key: "ChangYiMe13"
authors: ["Kai-Wei Chang","Wen-tau Yih","Chris Meek"]
year: 2013
booktitle: "EMNLP"
publish_type: conference
ispub: true
keyword: ["Natural Language Processing"]
paper_url: "http://research.microsoft.com/apps/pubs/?id=201352"
demo_url: "http://msrcstr.cloudapp.net/WordRelation.aspx"
slides_url: "../slides/ChangYiMe13_slide.pptx"
```
### Abstract
We present Multi-Relational Latent Semantic Analysis (MRLSA) which generalizes Latent Semantic Analysis (LSA). MRLSA provides an elegant approach to combining multiple relations between words by constructing a 3-way tensor. Similar to LSA, a low-rank approximation of the tensor is derived using a tensor decomposition. Each word in the vocabulary is thus represented by a vector in the latent semantic space and each relation is captured by a latent square matrix. The degree of two words having a specific relation can then be measured through simple linear algebraic operations. We demonstrate that by integrating multiple relations from both homogeneous and heterogeneous information sources, MRLSA achieves state-of-the-art performance on existing benchmark datasets for two relations, antonymy and is-a.

### Bib entry

> @inproceedings{ChangYiMe13, <br>
> author = {Kai-Wei Chang and Wen-tau Yih and Chris Meek}, <br>
> title= {{Multi-Relational Latent Semantic Analysis}}, <br>
> booktitle = {EMNLP}, <br>
> year = {2013} <br>
> }
