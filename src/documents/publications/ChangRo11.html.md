```
layout: publication
title: "Selective Block Minimization for Faster Convergence of Limited Memory Large-scale Linear Models"
key: "ChangRo11"
authors: ["Kai-Wei Chang","Dan Roth"]
year: 2011
booktitle: "KDD"
publish_type: conference
ispub: true
keyword: ["Large-Scale Learning","Machine Learning"]
paper_url: "http://cogcomp.cs.illinois.edu/page/publication_view/660"
software_url: "http://cogcomp.cs.illinois.edu/page/software_view/LMLM"
poster_url: "http://cogcomp.cs.illinois.edu/files/posters/sbm_kdd(1).pptx"
slides_url: "http://cogcomp.cs.illinois.edu/files/presentations/kdd_slide.pdf"
```
### Abstract
As the size of data sets used to build classifiers steadily increases, training a linear model efficiently with limited memory becomes essential. Several techniques deal with this problem by loading blocks of data from disk  one at a time, but usually take a considerable number of iterations to  converge to a reasonable model. Even the best block minimization techniques [1] require many block loads since they treat all training examples uniformly. As disk I/O is expensive, reducing the amount of disk access can dramatically decrease the training time.

### Bib entry

> @inproceedings{ChangRo11, <br>
> author = {Kai-Wei Chang and Dan Roth}, <br>
> title= {{Selective Block Minimization for Faster Convergence of Limited Memory Large-scale Linear Models}}, <br>
> booktitle = {KDD}, <br>
> year = {2011} <br>
> }
