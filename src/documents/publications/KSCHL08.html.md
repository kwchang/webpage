
```
layout: publication
title: "A Sequential Dual Method for Large Scale Multi-Class Linear SVMs"
key: "KSCHL08"
authors: ["S. Sathiya Keerthi", "S. Sundararajan", "Kai-Wei Chang","Cho-Jui Hsieh","Chih-Jen Lin"]
year: 2008
booktitle: "KDD"
publish_type: conference
ispub: true
keyword: ["Large-Scale Learning","Machine Learning"]
paper_url: "../papers/KSCHL08.pdf"
software_url: "http://www.csie.ntu.edu.tw/~cjlin/liblinear"
```
### Abstract
Efficient training of direct multi-class formulations of linear
Support Vector Machines is very useful in applications such
as text classification with a huge number examples as well
as features. This paper presents a fast dual method for this
training. The main idea is to sequentially traverse through
the training set and optimize the dual variables associated
with one example at a time. The speed of training is enhanced
further by shrinking and cooling heuristics. Experiments
indicate that our method is much faster than state of
the art solvers such as bundle, cutting plane and exponentiated
gradient methods

### Bib entry

> @inproceedings{KSCHL08, <br>
> author = {S. Sathiya Keerthi and S. Sundararajan and Kai-Wei Chang and Cho-Jui Hsieh and Chih-Jen Lin}, <br>
> title= {{A Sequential Dual Method for Large Scale Multi-Class Linear SVMs}}, <br>
> booktitle = {KDD}, <br>
> year = {2008} <br>
> }
