```
title: "Calendar"
layout: doc
```

To schedule a meeting with me, please use [this link](https://calendar.google.com/calendar/selfsched?sstoken=UUxxMHhGeERiZ0JRfGRlZmF1bHR8MmIyMzFlZWUwMzdjMGQ1Y2ZjMjVkZDdkN2NmNzg1MjY) or send an email to [meeting@kwchang.net](mailto:meeting@kwchang.net).

**Contact**
- email: kw AT kwchang DOT net <br>

## Calendar

<iframe src="https://calendar.google.com/calendar/embed?showTitle=0&amp;showPrint=0&amp;mode=WEEK&amp;height=600&amp;wkst=1&amp;bgcolor=%23ffffff&amp;src=kwchang.cs%40gmail.com&amp;color=%23b90e28&amp;src=changkw%40g.ucla.edu&amp;color=%238C500B&amp;src=orfnjqu2dpmgcjf6h244cn8d04%40group.calendar.google.com&amp;color=%230e61b9&amp;ctz=America%2FLos_Angeles" style="border-width:0" width="800" height="600" frameborder="0" scrolling="no"></iframe>
