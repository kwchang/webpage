```
title: "Application"
layout: doc
```
** Openings **
- Prospective PhD students in Computer Science: I am looking for 1~2 highly motivated PhD students starting at Fall 2019. Please apply directly to the UCLA CS program and indicate me as your potential faculty advisor. You are welcome to fill in the form below, but it is not required.  
- Prospective MS students in Computer Science: I am not in the admission committee. Please apply to the program directly. After you get the admission, I am happy to discuss with you about the opportunities.  
- Undergraduate summer visiting students: For students from China, the best way is through the CSST program: https://www.csst.ucla.edu/summer-program-csst. I may take 1~2 additional summer students.
- Current UCLA Undergraduate and Master student (MS thesis or Capstone Project): Please fill in the form below. I will contact you within around 1 week.


## Application Form

Please fill in the following form, if you are interested in joining my lab. 


<iframe src="https://docs.google.com/forms/d/e/1FAIpQLSdFofyH4GrGqbhhn8ZGfZtqc8ydYTXxKNh_TveJPjasq31Djw/viewform?embedded=true" width="640" height="2799" frameborder="0" marginheight="0" marginwidth="0">Loading...</iframe>

If you have trouble in submitting the form, please send it to kw+application AT kwchang Dot net.


### Response
Due to the large number of requests, I cannot respond or give feedback on your profile individually. For PhD applications, I will review applications and contact perspective students in the short list around February.
