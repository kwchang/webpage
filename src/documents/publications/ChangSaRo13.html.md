```
layout: publication
title: "A Constrained Latent Variable Model for Coreference Resolution"
key: "ChangSaRo13"
authors: ["Kai-Wei Chang","Rajhans Samdani","Dan Roth"]
year: 2013
booktitle: "EMNLP"
publish_type: conference
ispub: true
keyword: ["Natural Language Processing"]
paper_url: "http://cogcomp.cs.illinois.edu/page/publication_view/732"
demo_url: "http://cogcomp.cs.illinois.edu/page/demo_view/Coref"
poster_url: "http://cogcomp.cs.illinois.edu/files/posters/A%20Constrained%20Latent%20Variable%20Model%20for%20Coreference%20Resolution_poster.pptx"
```
### Abstract

Coreference resolution is a well known clustering task in Natural Language Processing. In this paper, we describe the Latent Left Linking model (L3M), a novel, principled, and linguistically motivated latent structured prediction approach to coreference resolution.

We show that L3M admits efficient inference and can be augmented with knowledge-based constraints; we also present a fast stochastic gradient based learning.

Experiments on ACE and Ontonotes data show that L3M and its constrained version, CL3M, are more accurate than several state-of-the-art approaches as well as some structured prediction models proposed in the literature.

### Bib entry

> @inproceedings{ChangSaRo13, <br>
> author = {Kai-Wei Chang and Rajhans Samdani and Dan Roth}, <br>
> title= {{A Constrained Latent Variable Model for Coreference Resolution}}, <br>
> booktitle = {EMNLP}, <br>
> year = {2013} <br>
> }
