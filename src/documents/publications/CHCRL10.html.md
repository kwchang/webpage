
```
layout: publication
title: "Training and Testing Low-degree Polynomial Data Mappings via Linear SVM"
key: "CHCRL10"
authors: ["Yin-Wen Chang","Cho-Jui Hsieh","Kai-Wei Chang","Michael Ringgaard","Chih-Jen Lin"]
year: 2010
booktitle: "JMLR"
publish_type: journal
ispub: true
keyword: ["Large-Scale Learning","Machine Learning"]
paper_url: "http://www.csie.ntu.edu.tw/~cjlin/papers/lowpoly_journal.pdf"
software_url: "http://www.csie.ntu.edu.tw/~cjlin/libsvmtools/#fast_training_testing_for_degree_2_polynomial_mappings_of_data"
exp_url: "http://www.csie.ntu.edu.tw/~cjlin/liblinear/exp.html#lowpoly_exp"
```
### Abstract
Kernel techniques have long been used in SVM to handle linearly inseparable problems by
transforming data to a high dimensional space, but training and testing large data sets
is often time consuming. In contrast, we can efficiently train and test much larger data
sets using linear SVM without kernels. In this work, we apply fast linear-SVM methods to
the explicit form of polynomially mapped data and investigate implementation issues. The
approach enjoys fast training and testing, but may sometimes achieve accuracy close to that
of using highly nonlinear kernels. Empirical experiments show that the proposed method
is useful for certain large-scale data sets. We successfully apply the proposed method to
a natural language processing (NLP) application by improving the testing accuracy under
some training/testing speed requirements.

### Bib entry

> @article{CCCRL10, <br>
> author = {Yin-Wen Chang and Cho-Jui Hsieh and Kai-Wei Chang and Michael Ringgaard and Chih-Jen Lin}, <br>
> title= {{Training and Testing Low-degree Polynomial Data Mappings via Linear SVM}}, <br>
> journal = {JMLR}, <br>
> year = {2010} <br>
> }
