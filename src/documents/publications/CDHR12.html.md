```
layout: publication
title: "Efficient Pattern-Based Time Series Classification on GPU "
key: "CDHR12"
authors: ["Kai-Wei Chang","Baplab Deka","W.-M. W. Hwu","Dan Roth"]
year: 2012
booktitle: "ICDM"
publish_type: conference
ispub: true
keyword: ["Data Mining"]
paper_url: "http://cogcomp.cs.illinois.edu/papers/undefined.pdf"
```

### Abstract
Time series shapelet discovery algorithm finds subsequences from a set of time series for use as primitives for time series classification. This algorithm has drawn a lot of interest because of the interpretability of its results. However, computation requirements restrict the algorithm from dealing with large data sets and may limit its application in many domains. In this paper, we address this issue by redesigning the algorithm for implementation on highly parallel Graphics Process Units (GPUs). We investigate several concepts of GPU programming and propose a dynamic programming algorithm that is suitable for implementation on GPUs. Results show that the proposed GPU implementation significantly reduces the running time of the shapelet discovery algorithm. For example, on the largest sample dataset from the original authors, the running time is reduced from half a day to two minutes. 

### Bib entry

> @inproceedings{CDHR12, <br>
> author = {Kai-Wei Chang and Baplab Deka and W.-M. W. Hwu and Dan Roth}, <br>
> title= {{Efficient Pattern-Based Time Series Classification on GPU }}, <br>
> booktitle = {ICDM}, <br>
> year = {2012} <br>
> }
