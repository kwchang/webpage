
```
layout: publication
title: "A Dual Coordinate Descent Method for Large-Scale Linear SVM"
key: "HCLKS08"
authors: ["Cho-Jui Hsieh","Kai-Wei Chang","Chih-Jen Lin","Sathia  S. Keerthi","S. Sundararajan"]
year: 2008
booktitle: "ICML"
publish_type: conference
ispub: true
iskeypub: true
keyword: ["Large-Scale Learning","Machine Learning"]
paper_url: "../papers/HCLKS08.pdf"
exp_url: "http://www.csie.ntu.edu.tw/~cjlin/liblinear/exp.html#dual_exp"
slides_url: "http://bilbo.cs.illinois.edu/~kchang10/icml_talk.pdf"
software_url: "http://www.csie.ntu.edu.tw/~cjlin/liblinear"
```
### Abstract
In many applications, data appear with a
huge number of instances as well as features.
Linear Support Vector Machines (SVM) is
one of the most popular tools to deal with
such large-scale sparse data. This paper
presents a novel dual coordinate descent
method for linear SVM with L1- and L2-
loss functions. The proposed method is simple and reaches an $\epsilon$-accurate solution in
$O(log(1/\epsilon))$ iterations. Experiments indicate
that our method is much faster than state
of the art solvers such as Pegasos, TRON,
SVMperf , and a recent primal coordinate descent implementation.

### Bib entry

> @inproceedings{HCLKS08, <br>
> author = {Cho-Jui Hsieh and Kai-Wei Chang and Chih-Jen Lin and Sathia  S. Keerthi and S. Sundararajan}, <br>
> title= {{A Dual Coordinate Descent Method for Large-Scale Linear SVM}}, <br>
> booktitle = {ICML}, <br>
> year = {2008} <br>
> }
