# Require
fsUtil = require('fs')
pathUtil = require('path')

siteUrl = 'http://www.cs.ucla.edu/~kwchang'
#siteUrl = 'http://10.160.72.198:9778'

# Prepare
textData =
	heading: ""
	copyright: '''

		'''

navigationData =
	top:
		'Papers': siteUrl+'/publications.html'
		'Teaching': siteUrl+'/teaching.html'
		'Talks': siteUrl+'/talks.html'
		'Calendar': siteUrl+'/calendar.html'
	course5901:
		'Overview': siteUrl+'/teaching/NLP16'
		'Syllabus': siteUrl+'/teaching/NLP16/syllabus.html'
		'Policies': siteUrl+'/teaching/NLP16/policies.html'
		'Presentations': siteUrl+'/teaching/NLP16/presentations.html'
		'Projects': siteUrl+'/teaching/NLP16/projects.html'
		'Resources': siteUrl+'/teaching/NLP16/resources.html'
		'Piazza': 'http://piazza.com/virginia/fall2016/cs6501004'
	courseSL:
		'Overview': siteUrl+'/teaching/SL17'
		'Syllabus': siteUrl+'/teaching/SL17/syllabus.html'
		'Policies': siteUrl+'/teaching/SL17/policies.html'
		'Presentations': siteUrl+'/teaching/SL17/presentations.html'
		'Projects': siteUrl+'/teaching/SL17/projects.html'
		'Resources': siteUrl+'/teaching/SL17/resources.html'
		'Piazza': 'https://piazza.com/virginia/spring2017/cs650101/home'
	bottom:
		'Home': siteUrl+'/index'

websiteVersion = require('./package.json').version
docpadVersion = require('./package.json').dependencies.docpad.toString().replace('~', '').replace('^', '')
#if process.env.NODE_ENV is 'production' then "http://docpad.org" else "http://localhost:9778"
contributorsGetter = null
contributors = null


# =================================
# Helpers

# Titles
getName = (a,b) ->
	if b is null
		return textData[b] ? humanize(b)
	else
		return textData[a][b] ? humanize(b)
getProjectName = (project) ->
	getName('projectNames',project)
getCategoryName = (category) ->
	getName('categoryNames',category)
getLinkName = (link) ->
	getName('linkNames',link)
getLabelName = (label) ->
	getName('labelNames',label)

# Humanize
humanize = (text) ->
	text ?= ''
	return require('underscore.string').humanize text.replace(/^[\-0-9]+/,'').replace(/\..+/,'')


# =================================
# Configuration


# The DocPad Configuration File
# It is simply a CoffeeScript Object which is parsed by CSON
docpadConfig =
	# =================================
	# DocPad Configuration
	ignoreHiddenFiles: true
	# Regenerate each day
	regenerateEvery: 1000*60*60*24


	# =================================
	# Template Data
	# These are variables that will be accessible via our templates

	templateData:

		# -----------------------------
		# Misc

		text: textData
		navigation: navigationData



		# -----------------------------
		# Site Properties

		site:
			fsUtil: fsUtil
			# The production URL of our website
			url: siteUrl

			# services
			services:
				googleAnalytics: "UA-74487606-1"

			# The default title of our website
			title: "Kai-Wei Chang"


			description: """
				"""

			# The website keywords (for SEO) separated by commas
			keywords: """
				"""

			# Styles
			styles: [
				siteUrl+'/vendor/normalize.css'
				siteUrl+'/vendor/h5bp.css'
				siteUrl+'/styles/style.css'
			].map (url) -> "#{url}?websiteVersion=#{websiteVersion}"

			# Script
			scripts: [
				# Vendor
				siteUrl+"/vendor/jquery.js"
				siteUrl+"/vendor/jquery-scrollto.js"
				siteUrl+"/vendor/modernizr.js"
				siteUrl+"/vendor/history.js"

				# Scripts
				siteUrl+"/vendor/historyjsit.js"
				siteUrl+"/scripts/bevry.js"
				siteUrl+"/scripts/script.js"
			].map (url) -> "#{url}?websiteVersion=#{websiteVersion}"

		# -----------------------------
		# Helper Functions
		getUrl: (document) -> 
			return @site.url + (document.url or document.get?('url'))

		# Names
		getName: getName
		getProjectName: getProjectName
		getCategoryName: getCategoryName
		getLinkName: getLinkName
		getLabelName: getLabelName

		# Get the prepared site/document title
		# Often we would like to specify particular formatting to our page's title
		# we can apply that formatting here
		getPreparedTitle: ->
			# if we have a title, we should use it suffixed by the site's title
			if @document.pageTitle isnt false and @document.title
				"#{@document.pageTitle or @document.title} | #{@site.title}"
			# if we don't have a title, then we should just use the site's title
			else if @document.pageTitle is false or @document.title? is false
				@site.title

		# Get the prepared site/document description
		getPreparedDescription: ->
			# if we have a document description, then we should use that, otherwise use the site's description
			@document.description or @site.description

		# Get the prepared site/document keywords
		getPreparedKeywords: ->
			# Merge the document keywords with the site keywords
			@site.keywords.concat(@document.keywords or []).join(', ')

		# Get Version
		getVersion: (v,places=1) ->
			return v.split('.')[0...places].join('.')

		# Read File
		readFile: (relativePath) ->
			path = @document.fullDirPath+'/'+relativePath
			result = fsUtil.readFileSync(path)
			if result instanceof Error
				throw result
			else
				return result.toString()

		# Code File
		codeFile: (relativePath,language) ->
			language ?= pathUtil.extname(relativePath).substr(1)
			contents = @readFile(relativePath)
			return """<pre><code class="#{language}">#{contents}</code></pre>"""

		# Get Contributors
		getContributors: -> contributors or []


	# =================================
	# Collections

	collections:

		# Fetch all documents that exist within the docs directory
		# And give them the following meta data based on their file structure
		# [\-0-9]+#{category}/[\-0-9]+#{name}.extension
		docs: (database) ->
			query =
				write: true
				relativeOutDirPath: $startsWith: 'learn/'
				body: $ne: ""
			sorting = [projectDirectory:1, categoryDirectory:1, filename:1]
			database.findAllLive(query, sorting).on 'add', (document) ->
				# Prepare
				a = document.attributes

				###
				learn/#{organisation}/#{project}/#{category}/#{filename}
				###
				pathDetailsExtractor = ///
					^
					.*?learn/
					(.+?)/        # organisation
					(.+?)/        # project
					(.+?)/        # category
					(.+?)\.       # basename
					(.+?)         # extension
					$
				///

				pathDetails = pathDetailsExtractor.exec(a.relativePath)

				# Properties
				layout = 'doc'
				standalone = true
				organisationDirectory = organisation = organisationName =
					projectDirectory = project = projectName =
					categoryDirectory = category = categoryName =
					title = pageTitle = null

				# Check if we are correctly structured
				if pathDetails?
					organisationDirectory = pathDetails[1]
					projectDirectory = pathDetails[2]
					categoryDirectory = pathDetails[3]
					basename = pathDetails[4]

					organisation = organisationDirectory.replace(/[\-0-9]+/, '')
					organisationName = humanize(project)

					project = projectDirectory.replace(/[\-0-9]+/, '')
					projectName = getProjectName(project)

					category = categoryDirectory.replace(/^[\-0-9]+/, '')
					categoryName = getCategoryName(category)

					name = basename.replace(/^[\-0-9]+/,'')

					title = "#{a.title or humanize name}"
					pageTitle = "#{title} | DocPad"  # changed from bevry website

					urls = ["docs/#{name}", "docs/#{category}-#{name}", "docpad/#{name}"]

					githubEditUrl = "https://github.com/#{organisationDirectory}/#{projectDirectory}/edit/master/"
					proseEditUrl = "http://prose.io/##{organisationDirectory}/#{projectDirectory}/edit/master/"
					editUrl = githubEditUrl + a.relativePath.replace("learn/#{organisationDirectory}/#{projectDirectory}/", '')

					# Apply
					document
						.setMetaDefaults({
							layout
							standalone

							name
							title
							pageTitle

							url: urls[0]

							editUrl

							organisationDirectory
							organisation
							organisationName

							projectDirectory
							project
							projectName

							categoryDirectory
							category
							categoryName
						})
						.addUrl(urls)

				# Otherwise ignore this document
				else
					console.log "The document #{a.relativePath} was at an invalid path, so has been ignored"
					document.setMetaDefaults({
						ignore: true
						render: false
						write: false
					})

		partners: (database) ->
			database.findAllLive({relativeOutDirPath:'learn/docpad/documentation/partners'}, [filename:1]).on 'add', (document) ->
				document.setMetaDefaults(write: false)


	# =================================
	# Plugins

	# =================================
	# Environments

	# Disable analytic services on the development environment
	environments:
		development:
			templateData:
				site:
					services:
						gauges: false
						googleAnalytics: false
						mixpanel: false
						reinvigorate: false


	# =================================
	# Events

	events:

		# Extend Template data
		#extendTemplateData: (opts) ->
			#opts.templateData.moment = require('moment')
			#opts.templateData.strUtil = require('underscore.string')

			# Return
			#return true

		# Generate Before
		generateBefore: (opts) ->
			# Reset contributors if we are a complete generation (not a partial one)
			contributors = null

			# Return
			return true

		# Fetch Contributors
		renderBefore: (opts,next) ->
			# Prepare
			docpad = @docpad

			# Check
			return next() 


# Export our DocPad Configuration
module.exports = docpadConfig
