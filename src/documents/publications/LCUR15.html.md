```
layout: publication
title: "Distributed Training of Structured SVM"
key: "LCUR1514"
authors: ["Ching-pei Lee","Kai-Wei Chang","Shyam Upadhyay","Dan Roth"]
year: 2015
booktitle: "OPT workshop at NIPS"
publish_type: conference
ispub: true
keyword: ["Large-Scale Learning","Machine Learning"]
paper_url: "http://opt-ml.org/papers/OPT2015_paper_1.pdf"
```
### Abstract
Training structured prediction models is time-consuming. However, most existing approaches only use a single machine, thus, the advantage of computing power and the capacity for larger data sets of multiple machines have not been exploited. In this work, we propose an efficient algorithm for distributedly training structured support vector machines based on a distributed block-coordinate descent method. Both theoretical and experimental results indicate that our method is efficient.

### Bib entry

> @inproceedings{LCUR15, <br>
> author = {Ching-pei Lee and Kai-Wei Chang and Shyam Upadhyay and Dan Roth}, <br>	
> title= {{Distributed Training of Structured SVM}}, <br>
> booktitle = {OPT workshop at NIPS}, <br>
> year = {2015} <br>
> }
