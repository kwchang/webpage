```
title: "Spring 2017 -- CS6501 Advanced Machine Learning"
layout: doc_courseML
coursename: "CS6501-Advanced ML"
```
## Announcements:
- 04.19.2017: [HW3](ML-hw3.pdf) release! The due date is 4/26/2017.
- 04.14.2017: Register your final project presentation [here](https://docs.google.com/spreadsheets/d/1ca-sfHerDfIg-wTydl1d9w9o_FdrZJxObNBV0RTzSkY/edit?usp=sharing). Final project will be due on 5/8 (Mon).
- 03.04.2017: [HW2](cs6501_hw2.pdf) release! The due date is 3/15/2017.
- 02.11.2017: [HW1](cs6501_ML_hw1.pdf) release! The due date is 2/18/2017.
- 02.05.2017: We will cancel the class on 2/8 and move the class to 2/17 4:00pm-5:15pm.
- 01.14.2017: Register your paper presentation at [here](https://docs.google.com/spreadsheets/d/1ZZRDd1-PRLhCPhkum9qysxI34SBLvUCRbgQY0vGtCyw/edit?usp=sharing)
- 01.09.2017: To students who want to take the class and were not able to register: Start attending the first few meetings of the class as if you are registered. We will maintain a waiting list and, given that some students will drop the class, some space will free up. There is no way to give any guarantees but, in the past, most of the interested students were able to get in.
- 01.09.2017: We will use [Piazza](https://piazza.com/virginia/spring2017/cs650101/home) as an online discussion platform. Please enroll.
- 01.09.2017: Welcome to CS6501-01 Advanced Machine Learning. 

## Course Information:
#### Lectures:
- Time: Mon/Wed 5:00pm -- 6:15pm. 
- Location: **Olsson Hall 009** 

#### Staff
- Instructor: [Kai-Wei Chang](http://kwchang.net), Email to: ml17@kwchang.net
  - Office hour: 2:00pm -- 3:00pm, Tue, or by appointment.
  - Office location: Room 412, Rice Hall
- TA: Wasi Ahmad. 
  - TA session: 5:00pm -- 6:00pm, Tue 
  - TA session location: TBA
  - Office location: Room 432, Rice Hall.
- TA: Md Rizwan Parvez.
  - Office hour: 2:00pm -- 3:00pm, Thu
  - Office location: Room 430, Rice Hall.
 

### Course Description
Machine Leaning (ML) enables us to use to program without explicitly enumerate every distinct cases. Recently, ML techniques have been widely used in many applications including machine translation, question answering, and extracting information from text.  In this course, we will cover some advanced elements and recent research trends in ML. 
The activities of the course include lectures, paper presentations, quizzes, homeworks, and a final project.

Tentative topics include:
- Machine learning background: binary and multi classification models, neural network reviews
- Sequential models: hidden Markov model, maximum entropy Markov model, conditional random fields
- General structured prediction models: graphical models, learning to search models 
- Inference algorithms: graph algorithms, integer liner programming, approximate inference, Lagrangian relaxation and dual decomposition
- Learning algorithms 
- Deep learning: CNN, recurrent NN LSTM, seq-to-seq models, structured deep learning.

#### Prerequisites
Students are expected to have taken a class in machine learning or related courses (e.g., AI, NLP, computer vision).
We will use linear algebra, probability, and calculus intensively. 
Programming experience is necessary for the final project.

#### [Textbook/references](resources.html)
- The following books are recommended but not required. Some course materials are derived from recent papers.  
  - Yoav Goldberg (2015), [A Primer on Neural Network Models for Natural Language Processing](http://u.cs.biu.ac.il/~yogo/nnlp.pdf)
  - Noah A. Smith (2011), [Linguistic Structure Prediction](http://www.cs.cmu.edu/~nasmith/LSP/)
  - Sebastian Nowozin and Christoph H. Lampert (2011), [Structured Learning and Prediction in Computer Vision](http://www.nowozin.net/sebastian/papers/nowozin2011structured-tutorial.pdf)
- Recommended reading list can be found at [here](resources.html). 
- The [ACL anthology](http://aclweb.org/anthology-new/) has a large collection of NLP papers. 

## [Schedule](syllabus.html)

## [Policies](policies.html)
- Grading policy: 15% Homeworks, 30% quizzes, 15% paper presentation, 40% final projects. See [here](policies.html) for details.
- [Honor Code](http://www.virginia.edu/honor/)


### [Presentations](presentations.html)

### [Projects](projects.html)

