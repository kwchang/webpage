```
title: "Fall 2016 -- CS6501 Natural Language Processing"
layout: doc_course5901
coursename: "CS6501-NLP"
```
## Announcements:
- 11.08.2016: Quiz3 solution is [here](doc/6501_16_quiz3-qn.pdf)
- 10.25.2016: Grade presentations using Collab!
- 10.18.2016: Quiz2 solution is [here](doc/6501_16_quiz2-qn.pdf)
- 10.11.2016: Check out your presentation date [here](https://docs.google.com/spreadsheets/d/13RC84Rqyqb8dCbhuJr-RV0zzw6qG8wFxTcmnRVkgFlY/edit?usp=sharing)
- 09.22.2016: Quiz1 solution is [here](doc/6501_16_quiz1-ans.pdf)
- 09.20.2016: Sign up your project team [here](https://docs.google.com/spreadsheets/d/1gbTKcis8X-s9Ssh_e1mlBoc__JCVsTm_qNEuZfJ7u94/edit#gid=0).
- 09.04.2016: Please see instructions of the [final project](projects.html). 
- 09.01.2016: Sign up your team and paper for presentation [here](https://docs.google.com/spreadsheets/d/13RC84Rqyqb8dCbhuJr-RV0zzw6qG8wFxTcmnRVkgFlY/edit?usp=sharing)
- 08.21.2016: To students who want to take the class and were not able to register: Start attending the first few meetings of the class as if you are registered. We will maintain a waiting list and, given that some students will drop the class, some space will free up. There is no way to give any guarantees but, in the past, most of the interested students were able to get in.
- 08.21.2016: We will use [Piazza](http://piazza.com/virginia/fall2016/cs6501004) as an online discussion platform. Please enroll.
- 08.21.2016: Welcome to CS6501-004 Natural Language Processing. 

## Course Information:
#### Lectures:
- Time: Tue/Thu 12:30pm -- 1:45pm. 
- Location: **Thornton Hall D223** (Note: classroom change)

#### Staff
- Instructor: [Kai-Wei Chang](http://kwchang.net), Email to: nlp16@kwchang.net
  - Office hour: 2:00pm -- 3:00pm, Tue, or by appointment.
  - Office location: Room 412, Rice Hall
- TA: Wasi Ahmad. 
  - Office hour: 4:00pm -- 5:00pm, Mon
  - Office location: Room 432.

### Course Description
Natural language processing (NLP) enables computers to use and understand human languages. Recently, NLP techniques have been widely used in many applications including machine translation, question answering, and extracting information from text.  In this course, we will cover the fundamental elements and recent research trends in NLP. Tentative topics include syntactic analysis, semantic analysis, and NLP applications as well as the underlying machine learning methods that widely used in modeling NLP systems. The activities of the course include lectures, paper presentations, quizzes, a critical review report, and a final project.

Tentative topics include:
- Machine learning background: linear classification models, basic structured prediction models
- Syntactic analysis: part-of-speech tagging, chunking, dependency parsing, constituency parsing.
- Semantics: brown clusters, vector-space semantics, semantic role labeling. 
- NLP Applications: name entity recognition, machine translation, information extraction.

#### Prerequisites
Students are expected to have taken a class in linear algebra and in probability and statistics and a basic class in theory of computation and algorithms. 
Programming experience is necessary for the final project.

#### [Textbook/references](resources.html)
- The following textbook is recommended but not required. 
  -  Jurafsky and Martin (2008), Speech and Language Processing, 2nd edition. 
- Recommended reading list can be found at [here](resources.html). 
- The [ACL anthology](http://aclweb.org/anthology-new/) has a large collection of NLP papers. 

## [Schedule](syllabus.html)

## [Policies](policies.html)
- Grading policy: 50% final project, 25% quizzes, 10% crticial review, 15% paper presentation. See [here](policies.html) for details.
- [Honor Code](http://www.virginia.edu/honor/)


### [Presentations](presentations.html)

### [Projects](projects.html)

