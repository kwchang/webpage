```
title: "Policies"
layout: doc_course5901
coursename: "CS6501-NLP"
```

## Grading Policy

Each student's grade is calculated as follows:
- 50% final project (35% implementation + report writing, 15% presentation)
- 25% quizzes (~8% each)
- 10% critical review
- 15% paper presentation
- ??% Bonus

The cut-off points for the letter grades will no higher than the numbers listed in 
the [default threshold](http://its.virginia.edu/sis/grading/gradethresholds.html) (see the first section).
That is, you may get A+ with a raw score of 94.3, and you are guaranteed to get A if your raw score is 95.
We do not round grades (i.e., 89.999 gets B+ by default), but we may curve grades. 


## Late Policy
All assignments and reports must be submitted to Collab by the time and date indicated. 
Every student has a credit of 48 hours for all the assignments including the final project. 
No assignment will be accepted after the credit is run out.
For group projects, the credits of late hours cannot be added up.
Do let us know if there are exceptional situations where this lenient policy is not satisfactory.

## Accommodations for students with disabilities
To insure that disability-related concerns are properly addressed from the beginning, 
students with disabilities who require assistance to participate in this class 
(including exam accommodations) are asked to see me as soon as possible.
We aim to be as accommodating and fair as possible.

## Collaboration and use of outside resources
Discussion and collaboration are encouraged. However, students must write their own reports.
When using outside resources, proper citation is necessary. This includes papers, text books, softwares, websites, and helps from others. 
For the details of Honor Code, please refer to [UVA Honor Committee](http://www.virginia.edu/honor/).
If you have any doubt, please check with me in advance. You may get F in the final letter grade if we detect any cheating. 


