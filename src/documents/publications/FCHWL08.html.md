```
layout: publication
title: "LIBLINEAR: A Library for Large Linear Classification"
key: "FCHWL08"
authors: ["Rong En Fan","Kai-Wei Chang","Cho-Jui Hsieh","X.-R. Wang","Chih-Jen Lin"]
year: 2008
booktitle: "JMLR"
publish_type: journal
ispub: true
keyword: ["Large-Scale Learning","Machine Learning","Software"]
paper_url: "../papers/FCHWL08.pdf"
software_url: "http://www.csie.ntu.edu.tw/~cjlin/liblinear"
```
### Abstract
LIBLINEAR is an open source library for large-scale linear classification. It supports logistic
regression and linear support vector machines. We provide easy-to-use command-line tools
and library calls for users and developers. Comprehensive documents are available for both
beginners and advanced users. Experiments demonstrate that LIBLINEAR is very efficient
on large sparse data sets.

### Bib entry

> @article{FCHWL08, <br>
> author = {Rong En Fan and Kai-Wei Chang and Cho-Jui Hsieh and X.-R. Wang and Chih-Jen Lin}, <br>
> title= {{LIBLINEAR: A Library for Large Linear Classification}}, <br>
> journal = {JMLR}, <br>
> year = {2008} <br>
> }
